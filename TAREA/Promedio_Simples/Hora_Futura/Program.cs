﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main()
    { 
        DateTime horaActual = DateTime.Now;
        int horasAgregadas = 5;        
        DateTime horaFutura = horaActual.AddHours(horasAgregadas);
        Console.WriteLine("Hora Actual: " + horaActual);
        Console.WriteLine("Hora Futura: " + horaFutura);
        Console.ReadLine();
    }
}

