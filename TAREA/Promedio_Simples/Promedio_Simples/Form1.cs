﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Promedio_Simples
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCalcularPromedio_Click(object sender, EventArgs e)
        {
            try
            {
                // Obtener las notas ingresadas y convertirlas a números
                double nota1 = double.Parse(txtNota1.Text);
                double nota2 = double.Parse(txtNota2.Text);
                double nota3 = double.Parse(txtNota2.Text);

                // Calcular el promedio
                double promedio = (nota1 + nota2 + nota3) / 3;

                // Mostrar el resultado en un cuadro de mensaje
                MessageBox.Show($"El promedio es: {promedio}", "Resultado");
            }
            catch (FormatException)
            {
                MessageBox.Show("Por favor, ingresa notas válidas.", "Error");
            }
        }
    }
}
