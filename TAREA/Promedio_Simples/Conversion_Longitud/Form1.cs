﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversion_Longitud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                double cantidadMetros = double.Parse(txtMetros.Text);              
                double cantidadPies = cantidadMetros * 3.28084;
                lblResultado.Text = $"Resultado: {cantidadPies} pies";
            }
            catch (FormatException)
            {
                MessageBox.Show("Por favor, ingresa una cantidad válida en metros.", "Error");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                double cantidadMetros = double.Parse(txtMetros.Text);
                double cantidadCentimetros = cantidadMetros * 100;
                lblResultado.Text = $"Resultado: {cantidadCentimetros} centímetros";
            }
            catch (FormatException)
            {
                MessageBox.Show("Por favor, ingresa una cantidad válida en metros.", "Error");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                double cantidadMetros = double.Parse(txtMetros.Text);
                double cantidadKilometros = cantidadMetros * 0.001;
                lblResultado.Text = $"Resultado: {cantidadKilometros} kilómetros";
            }
            catch (FormatException)
            {
                MessageBox.Show("Por favor, ingresa una cantidad válida en metros.", "Error");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                // Obtener la cantidad en metros ingresada
                double cantidadMetros = double.Parse(txtMetros.Text);

                // Realizar la conversión a pulgadas (1 metro = 39.3701 pulgadas)
                double cantidadPulgadas = cantidadMetros * 39.3701;

                // Mostrar el resultado en una etiqueta
                lblResultado.Text = $"Resultado: {cantidadPulgadas} pulgadas";
            }
            catch (FormatException)
            {
                MessageBox.Show("Por favor, ingresa una cantidad válida en metros.", "Error");
            }
        }
    }
}
