CREATE TABLE Estudiantes (
  IdEstudiante INT AUTO_INCREMENT PRIMARY KEY,
  Nombre VARCHAR(255) NOT NULL,
  Grado VARCHAR(50) NOT NULL
);

CREATE TABLE Materias (
  IdMateria INT AUTO_INCREMENT PRIMARY KEY,
  NombreMateria VARCHAR(255) NOT NULL
);

CREATE TABLE Notas (
  IdNota INT AUTO_INCREMENT PRIMARY KEY,
  IdEstudiante INT NOT NULL,
  IdMateria INT NOT NULL,
  Puntaje DECIMAL(9,2) NOT NULL,
  FOREIGN KEY (IdEstudiante) REFERENCES Estudiantes(IdEstudiante) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (IdMateria) REFERENCES Materias(IdMateria) ON DELETE CASCADE ON UPDATE CASCADE
);
-- Procedimiento almacenado para listar todas las notas
DELIMITER //
CREATE PROCEDURE sp_listar_notas()
BEGIN
    SELECT * FROM Notas;
END;
//

-- Procedimiento almacenado para insertar una nota
DELIMITER //
CREATE PROCEDURE sp_insertar_nota(IN id_estudiante INT, IN id_materia INT, IN nota DECIMAL(5, 2))
BEGIN
    INSERT INTO Notas (IdEstudiante, IdMateria, Nota) VALUES (id_estudiante, id_materia, nota);
END;
//

-- Procedimiento almacenado para actualizar una nota
DELIMITER //
CREATE PROCEDURE sp_actualizar_nota(IN id_nota INT, IN id_estudiante INT, IN id_materia INT, IN nota DECIMAL(5, 2))
BEGIN
    UPDATE Notas
    SET IdEstudiante = id_estudiante, IdMateria = id_materia, Nota = nota
    WHERE IdNota = id_nota;
END;
//

-- Procedimiento almacenado para eliminar una nota
DELIMITER //
CREATE PROCEDURE sp_eliminar_nota(IN id_nota INT)
BEGIN
    DELETE FROM Notas WHERE IdNota = id_nota;
END;
//


-- Procedimiento almacenado para listar todos los estudiantes
DELIMITER //
CREATE PROCEDURE sp_listar_estudiantes()
BEGIN
    SELECT * FROM Estudiantes;
END;
//

-- Procedimiento almacenado para insertar un estudiante
DELIMITER //
CREATE PROCEDURE sp_insertar_estudiante(IN nombre VARCHAR(255), IN grado VARCHAR(255), IN asignarMateria VARCHAR(255))
BEGIN
    INSERT INTO Estudiantes (Nombre, Grado, AsignarMateria) VALUES (nombre, grado, asignarMateria);
END;
//

-- Procedimiento almacenado para actualizar un estudiante
DELIMITER //
CREATE PROCEDURE sp_actualizar_estudiante(IN id_estudiante INT, IN nombre VARCHAR(255), IN grado VARCHAR(255), IN asignarMateria VARCHAR(255))
BEGIN
    UPDATE Estudiantes
    SET Nombre = nombre, Grado = grado, AsignarMateria = asignarMateria
    WHERE IdEstudiante = id_estudiante;
END;
//

-- Procedimiento almacenado para eliminar un estudiante
DELIMITER //
CREATE PROCEDURE sp_eliminar_estudiante(IN id_estudiante INT)
BEGIN
    DELETE FROM Estudiantes WHERE IdEstudiante = id_estudiante;
END;
//

-- Procedimiento almacenado para listar todas las materias
DELIMITER //
CREATE PROCEDURE sp_listar_materias()
BEGIN
    SELECT * FROM Materias;
END;
//

-- Procedimiento almacenado para insertar una materia
DELIMITER //
CREATE PROCEDURE sp_insertar_materia(IN nombreMateria VARCHAR(255))
BEGIN
    INSERT INTO Materias (NombreMateria) VALUES (nombreMateria);
END;
//

-- Procedimiento almacenado para actualizar una materia
DELIMITER //
CREATE PROCEDURE sp_actualizar_materia(IN id_materia INT, IN nombreMateria VARCHAR(255))
BEGIN
    UPDATE Materias
    SET NombreMateria = nombreMateria
    WHERE IdMateria = id_materia;
END;
//

-- Procedimiento almacenado para eliminar una materia
DELIMITER //
CREATE PROCEDURE sp_eliminar_materia(IN id_materia INT)
BEGIN
    DELETE FROM Materias WHERE IdMateria = id_materia;
END;
//
