﻿using System;
using System.Data;
using Sistema.Datos;
using Sistema.Entidades;

namespace Sistema.Negocio
{
    public class NNotas
    {
        public static void Guardar(ENotas notas)
        {
            DNotas datos = new DNotas();
            datos.Guardar(notas);
        }

        public static void Modificar(ENotas notas)
        {
            DNotas datos = new DNotas();
            datos.Modificar(notas);
        }

        public static void Eliminar(int idNotas)
        {
            DNotas datos = new DNotas();
            datos.Eliminar(idNotas);
        }

        public static ENotas Buscar(int idNotas)
        {
            DNotas datos = new DNotas();
            return datos.Buscar(idNotas);
        }
    }
}
