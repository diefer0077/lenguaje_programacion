﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sistema.Datos;
using Sistema.Entidades;


namespace Sistema.Negocio
{
    public class NEstudiante
    {
        
        public static DataTable Listar()
        {
            DEstudiante datos = new DEstudiante();
            return datos.Listar();
        }

       
        public static void Insertar(string nombre, string grado, string asignarMateria)
        {
            DEstudiante datos = new DEstudiante();
            datos.Insertar(nombre, grado, asignarMateria);
        }

        
        public static void Modificar(int idEstudiante, string nombre, string grado, string asignarMateria)
        {
            DEstudiante datos = new DEstudiante();
            datos.Actualizar(idEstudiante, nombre, grado, asignarMateria);
        }

        
        public static void Eliminar(int idEstudiante)
        {
            DEstudiante datos = new DEstudiante();
            datos.Eliminar(idEstudiante);
        }
        public static EEstudiante Buscar(int idEstudiante)
        {
            DEstudiante datos = new DEstudiante();
            return datos.Buscar(idEstudiante);
        }
        public static void Guardar(int idEstudiante, string nombre, string grado, string asignarMateria)

        {
            if (idEstudiante > 0)
            {
                
                Modificar(idEstudiante, nombre, grado, asignarMateria);
            }
            else
            {
               
                Insertar(nombre, grado, asignarMateria);
            }
        }

    }
}
