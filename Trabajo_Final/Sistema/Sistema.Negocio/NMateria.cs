﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Sistema.Datos;
using Sistema.Entidades;

namespace Sistema.Negocio
{
    public class NMateria
    {
        
        public static DataTable Listar()
        {
            DMateria datos = new DMateria();
            return datos.Listar();
        }

       
        public static void Insertar(string nombreMateria)
        {
            DMateria datos = new DMateria();
            datos.Insertar(nombreMateria);
        }

     
        public static void Modificar(int idMateria, string nombreMateria)
        {
            DMateria datos = new DMateria();
            datos.Actualizar(idMateria, nombreMateria);
        }

      
        public static void Eliminar(int idMateria)
        {
            DMateria datos = new DMateria();
            datos.Eliminar(idMateria);
        }
        public static void Guardar(int idMateria, string nombreMateria)
        {
            if (idMateria > 0)
            {
                
                Modificar(idMateria, nombreMateria);
            }
            else
            {
                
                Insertar(nombreMateria);
            }
        }

        public static EMateria Buscar(int idMateria)
        {
            throw new NotImplementedException();
        }
    }
}
