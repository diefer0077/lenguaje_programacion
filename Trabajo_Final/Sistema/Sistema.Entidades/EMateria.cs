﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Entidades
{
    public class EMateria
    {
        public int IdMateria { get; set; }
        public string NombreMateria { get; set; }
    }
}
