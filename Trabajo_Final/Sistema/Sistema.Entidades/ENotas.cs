﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Entidades
{
    public class ENotas
    {
        public int IdNota { get; set; }
        public int IdEstudiante { get; set; }
        public int IdMateria { get; set; }
        public double Nota { get; set; }
        public object IdNotas { get; set; }
    }
}
