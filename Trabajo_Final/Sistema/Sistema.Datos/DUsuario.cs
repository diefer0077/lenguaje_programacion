﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Sistema.Datos
{
    public class DUsuario
    {
        private MySqlConnection GetConnection()
        {
            return Conexion.getInstancia().CrearConexion();
        }

        public DataTable Login(string Email, string Clave)
        {
            DataTable Tabla = new DataTable();
            MySqlConnection SqlCon = new MySqlConnection();

            try
            {
                SqlCon = Conexion.getInstancia().CrearConexion();
                MySqlCommand Comando = new MySqlCommand("usuario_login", SqlCon); 
                Comando.CommandType = CommandType.StoredProcedure;
                Comando.Parameters.Add("@p_email", MySqlDbType.VarChar).Value = Email; 
                Comando.Parameters.Add("@p_clave", MySqlDbType.VarChar).Value = Clave;
                SqlCon.Open();
                MySqlDataReader Resultado = Comando.ExecuteReader();
                Tabla.Load(Resultado);
                return Tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
        }


        public string Existe(string Valor)
        {
            using (MySqlConnection SqlCon = GetConnection())
            {
                try
                {
                    using (MySqlCommand Comando = new MySqlCommand("usuario_existe", SqlCon))
                    {
                        Comando.CommandType = CommandType.StoredProcedure;
                        Comando.Parameters.Add("@valor", MySqlDbType.VarChar).Value = Valor;
                        MySqlParameter ParExiste = new MySqlParameter();
                        ParExiste.ParameterName = "@existe";
                        ParExiste.DbType = DbType.Boolean;
                        ParExiste.Direction = ParameterDirection.Output;
                        Comando.Parameters.Add(ParExiste);
                        SqlCon.Open();
                        Comando.ExecuteNonQuery();
                        return Convert.ToBoolean(ParExiste.Value) ? "OK" : "No se pudo activar el registro";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        public string Activar(int Id)
        {
            using (MySqlConnection SqlCon = GetConnection())
            {
                try
                {
                    using (MySqlCommand Comando = new MySqlCommand("usuario_activar", SqlCon))
                    {
                        Comando.CommandType = CommandType.StoredProcedure;
                        Comando.Parameters.Add("@idusuario", MySqlDbType.Int32).Value = Id;
                        SqlCon.Open();
                        int filasAfectadas = Comando.ExecuteNonQuery();
                        return filasAfectadas > 0 ? "OK" : "No se pudo activar el registro";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        public string Desactivar(int Id)
        {
            using (MySqlConnection SqlCon = GetConnection())
            {
                try
                {
                    using (MySqlCommand Comando = new MySqlCommand("usuario_desactivar", SqlCon))
                    {
                        Comando.CommandType = CommandType.StoredProcedure;
                        Comando.Parameters.Add("@idusuario", MySqlDbType.Int32).Value = Id;
                        SqlCon.Open();
                        int filasAfectadas = Comando.ExecuteNonQuery();
                        return filasAfectadas > 0 ? "OK" : "No se pudo desactivar el usuario";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }
    }
}
