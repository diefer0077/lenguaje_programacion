﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;

namespace Sistema.Datos
{
    public class ConexionCRUD
    {
        private string cadenaConexion;
        private static ConexionCRUD instanciaConexion = null;

        public ConexionCRUD()
        {

            cadenaConexion = ConfigurationManager.ConnectionStrings["ConexionCRUD"].ConnectionString;
        }

        public MySqlConnection CrearConexion()
        {
            MySqlConnection conexion = new MySqlConnection(cadenaConexion);

            try
            {
                conexion.Open();
                catch (Exception ex)
            {
                conexion = null;
                throw ex;
            }
            return conexion;
        }

        public static ConexionCRUD ObtenerInstancia()
        {
            if (instanciaConexion == null)
            {
                instanciaConexion = new ConexionCRUD();
            }
            return instanciaConexion;
        }
    }
}
