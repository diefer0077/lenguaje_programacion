﻿using System;
using System.Data;
using MySql.Data.MySqlClient; 
using Sistema.Entidades;

namespace Sistema.Datos
{
    public class DNotas
    {
        private Conexion conexion = Conexion.getInstancia(); 
     
        public DataTable Listar()
        {
            DataTable tabla = new DataTable();
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_listar_notas", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(tabla);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
            return tabla;
        }

        public string Insertar(ENotas nota)
        {
            string respuesta = "";
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_insertar_nota", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_estudiante", nota.IdEstudiante);
                cmd.Parameters.AddWithValue("@id_materia", nota.IdMateria);
                cmd.Parameters.AddWithValue("@nota", nota.Nota);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                respuesta = "Nota registrada correctamente";
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
            return respuesta;
        }


        public string Guardar(ENotas nota)
        {
            string respuesta = "";
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_actualizar_nota", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_nota", nota.IdNota);
                cmd.Parameters.AddWithValue("@id_estudiante", nota.IdEstudiante);
                cmd.Parameters.AddWithValue("@id_materia", nota.IdMateria);
                cmd.Parameters.AddWithValue("@nota", nota.Nota);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                respuesta = "Nota actualizada correctamente";
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
            return respuesta;
        }

        
        public string Eliminar(int id)
        {
            string respuesta = "";
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_eliminar_nota", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_nota", id);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                respuesta = "Nota eliminada correctamente";
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
            return respuesta;
        }

        public void Insertar(int idEstudiante, int idMateria, double nota)
        {
            throw new NotImplementedException();
        }

        public void Guardar(int idNota, int idEstudiante, int idMateria, double nota)
        {
            throw new NotImplementedException();
        }
        public void Modificar(ENotas notas)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("UPDATE Notas SET IdEstudiante = @idEstudiante, IdMateria = @idMateria, Nota = @nota WHERE IdNotas = @idNotas", sqlConnection);
                cmd.Parameters.AddWithValue("@idNotas", notas.IdNotas);
                cmd.Parameters.AddWithValue("@idEstudiante", notas.IdEstudiante);
                cmd.Parameters.AddWithValue("@idMateria", notas.IdMateria);
                cmd.Parameters.AddWithValue("@nota", notas.Nota);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }
        public void Guardar(ENotas notas)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO Notas (IdEstudiante, IdMateria, Nota) VALUES (@idEstudiante, @idMateria, @nota)", sqlConnection);
                cmd.Parameters.AddWithValue("@idEstudiante", notas.IdEstudiante);
                cmd.Parameters.AddWithValue("@idMateria", notas.IdMateria);
                cmd.Parameters.AddWithValue("@nota", notas.Nota);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }
    }
}
