﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Datos
{
    public class Conexion
    {
        private string servidor;
        private string baseDatos;
        private string usuario;
        private string contrasenia;
        private int puerto;
        private static Conexion instanciaConexion = null;

        public Conexion()
        {
        
            this.servidor = "localhost";
            this.baseDatos = "dbsistema";
            this.usuario = "root";
            this.contrasenia = "admin";
            this.puerto = 3306; 
        }

        public MySqlConnection CrearConexion()
        {
            MySqlConnection cadenaConexion = new MySqlConnection();
            
            
            try
            {
                cadenaConexion.ConnectionString = "Server=" + this.servidor + "; Port=" + this.puerto + "; Database=" + this.baseDatos + "; User ID=" + this.usuario + "; Password=" + this.contrasenia;
            }
            catch (Exception ex)
            {
                cadenaConexion = null;
                throw ex;
            }
            return cadenaConexion;
        }

        public static Conexion getInstancia()
        {
            if (instanciaConexion == null)
            {
                instanciaConexion = new Conexion();
            }
            return instanciaConexion;
        }
    }
}
