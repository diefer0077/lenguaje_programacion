﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using Sistema.Entidades;

namespace Sistema.Datos
{
    public class DMateria
    {
        private Conexion conexion = Conexion.getInstancia();

        // Método para listar todas las materias
        public DataTable Listar()
        {
            DataTable tabla = new DataTable();
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_listar_materias", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(tabla);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
            return tabla;
        }

        // Método para insertar una materia
        public void Insertar(string nombreMateria)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_insertar_materia", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                cmd.Parameters.AddWithValue("@nombreMateria", nombreMateria);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }

        // Método para actualizar una materia
        public void Actualizar(int idMateria, string nombreMateria)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_actualizar_materia", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                cmd.Parameters.AddWithValue("@idMateria", idMateria);
                cmd.Parameters.AddWithValue("@nombreMateria", nombreMateria);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }

      
        public void Eliminar(int idMateria)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_eliminar_materia", sqlConnection); 
                cmd.CommandType = CommandType.StoredProcedure; 
                cmd.Parameters.AddWithValue("@idMateria", idMateria);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }
    }
}
