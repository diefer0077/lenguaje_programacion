﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using Sistema.Entidades;

namespace Sistema.Datos
{
    public class DEstudiante
    {
        private Conexion conexion = Conexion.getInstancia();

        // Método para listar todos los estudiantes
        public DataTable Listar()
        {
            DataTable tabla = new DataTable();
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_listar_estudiantes", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(tabla);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
            return tabla;
        }

        // Método para insertar un estudiante
        public void Insertar(string nombre, string grado, string asignarMateria)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_insertar_estudiante", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@grado", grado);
                cmd.Parameters.AddWithValue("@asignarMateria", asignarMateria);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }

        // Método para actualizar un estudiante
        public void Actualizar(int idEstudiante, string nombre, string grado, string asignarMateria)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_actualizar_estudiante", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                cmd.Parameters.AddWithValue("@idEstudiante", idEstudiante);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@grado", grado);
                cmd.Parameters.AddWithValue("@asignarMateria", asignarMateria);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }

        // Método para eliminar un estudiante
        public void Eliminar(int idEstudiante)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("sp_eliminar_estudiante", sqlConnection); // Utiliza el procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure; // Indica que es un procedimiento almacenado
                cmd.Parameters.AddWithValue("@idEstudiante", idEstudiante);
                sqlConnection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }
        public EEstudiante Buscar(int idEstudiante)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            try
            {
                sqlConnection = conexion.CrearConexion();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM Estudiantes WHERE IdEstudiante = @idEstudiante", sqlConnection);
                cmd.Parameters.AddWithValue("@idEstudiante", idEstudiante);
                sqlConnection.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EEstudiante estudiante = new EEstudiante
                    {
                        IdEstudiante = Convert.ToInt32(reader["IdEstudiante"]),
                        Nombre = reader["Nombre"].ToString(),
                        Grado = reader["Grado"].ToString(),
                        AsignarMateria = reader["AsignarMateria"].ToString()
                    };
                    return estudiante;
                }
                return null; 
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close();
            }
        }
    }
}
