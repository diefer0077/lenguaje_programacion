﻿using System;
using System.Windows.Forms;
using Sistema.Negocio;
using Sistema.Entidades;

namespace Sistema.Presentacion
{
    public partial class FrmNotas : Form
    {
        public FrmNotas()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ENotas notas = new ENotas
                {
                    IdEstudiante = Convert.ToInt32(TxtIdEstudiante.Text),
                    IdMateria = Convert.ToInt32(TxtIdMateria.Text),
                    Nota = Convert.ToDouble(TxtNotas.Text)
                };

                NNotas.Guardar(notas);
                MessageBox.Show("Nota guardada exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar la nota: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ENotas notas = new ENotas
                {
                    
                    IdEstudiante = Convert.ToInt32(TxtIdEstudiante.Text),
                    IdMateria = Convert.ToInt32(TxtIdMateria.Text),
                    Nota = Convert.ToDouble(TxtNotas.Text)
                };

                NNotas.Modificar(notas);
                MessageBox.Show("Nota modificada exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al modificar la nota: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                int idNotas = Convert.ToInt32(TxtIdNotas.Text);
                NNotas.Eliminar(idNotas);
                MessageBox.Show("Nota eliminada exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al eliminar la nota: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                int idNotas = Convert.ToInt32(TxtIdNotas.Text);
                ENotas notas = NNotas.Buscar(idNotas);

                if (notas != null)
                {
                    TxtIdEstudiante.Text = notas.IdEstudiante.ToString();
                    TxtIdMateria.Text = notas.IdMateria.ToString();
                    TxtNotas.Text = notas.Nota.ToString();
                }
                else
                {
                    MessageBox.Show("Nota no encontrada", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar la nota: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            TxtIdNotas.Text = "";
            TxtIdEstudiante.Text = "";
            TxtIdMateria.Text = "";
            TxtNotas.Text = "";
        }

        private void FrmNotas_Load(object sender, EventArgs e)
        {

        }

        private void TxtNotas_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
