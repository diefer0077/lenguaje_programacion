﻿namespace Sistema.Presentacion
{
    partial class FrmCrud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnEstudiante = new System.Windows.Forms.Button();
            this.BtnMateria = new System.Windows.Forms.Button();
            this.BtnNota = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnEstudiante
            // 
            this.BtnEstudiante.Location = new System.Drawing.Point(33, 134);
            this.BtnEstudiante.Name = "BtnEstudiante";
            this.BtnEstudiante.Size = new System.Drawing.Size(218, 93);
            this.BtnEstudiante.TabIndex = 0;
            this.BtnEstudiante.Text = "Estudiante";
            this.BtnEstudiante.UseVisualStyleBackColor = true;
            this.BtnEstudiante.Click += new System.EventHandler(this.BtnEstudiante_Click);
            // 
            // BtnMateria
            // 
            this.BtnMateria.Location = new System.Drawing.Point(282, 134);
            this.BtnMateria.Name = "BtnMateria";
            this.BtnMateria.Size = new System.Drawing.Size(251, 94);
            this.BtnMateria.TabIndex = 1;
            this.BtnMateria.Text = "Materia";
            this.BtnMateria.UseVisualStyleBackColor = true;
            this.BtnMateria.Click += new System.EventHandler(this.BtnMateria_Click);
            // 
            // BtnNota
            // 
            this.BtnNota.Location = new System.Drawing.Point(567, 134);
            this.BtnNota.Name = "BtnNota";
            this.BtnNota.Size = new System.Drawing.Size(208, 93);
            this.BtnNota.TabIndex = 2;
            this.BtnNota.Text = "Nota";
            this.BtnNota.UseVisualStyleBackColor = true;
            this.BtnNota.Click += new System.EventHandler(this.BtnNota_Click);
            // 
            // FrmCrud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnNota);
            this.Controls.Add(this.BtnMateria);
            this.Controls.Add(this.BtnEstudiante);
            this.Name = "FrmCrud";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmCrud_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnEstudiante;
        private System.Windows.Forms.Button BtnMateria;
        private System.Windows.Forms.Button BtnNota;
    }
}