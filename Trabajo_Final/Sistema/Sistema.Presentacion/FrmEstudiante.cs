﻿using System;
using System.Windows.Forms;
using Sistema.Entidades;
using Sistema.Negocio;
namespace Sistema.Presentacion
{
    public partial class FrmEstudiante : Form
    {
        public FrmEstudiante()
        {
            InitializeComponent();
        }
        NEstudiante estudiante = new NEstudiante();
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                EEstudiante estudiante = new EEstudiante
                {
                    Nombre = TxtNombre.Text,
                    Grado = TxtGrado.Text,
                    AsignarMateria = TxtAsingnarMateria.Text
                };

                NEstudiante.Guardar(0, TxtNombre.Text, TxtGrado.Text, TxtAsingnarMateria.Text);
                MessageBox.Show("Estudiante guardado exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar el estudiante: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int idEstudiante = Convert.ToInt32(TxtIdEstudiante.Text);
                string nombre = TxtNombre.Text;
                string grado = TxtGrado.Text;
                string asignarMateria = TxtAsingnarMateria.Text;

                NEstudiante.Modificar(idEstudiante, nombre, grado, asignarMateria);
                MessageBox.Show("Estudiante modificado exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al modificar el estudiante: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                int idEstudiante = Convert.ToInt32(TxtIdEstudiante.Text);
                NEstudiante.Eliminar(idEstudiante);
                MessageBox.Show("Estudiante eliminado exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al eliminar el estudiante: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                int idEstudiante = Convert.ToInt32(TxtIdEstudiante.Text);
                EEstudiante estudiante = NEstudiante.Buscar(idEstudiante);

                if (estudiante != null)
                {
                    TxtNombre.Text = estudiante.Nombre;
                    TxtGrado.Text = estudiante.Grado;
                    TxtAsingnarMateria.Text = estudiante.AsignarMateria;
                }
                else
                {
                    MessageBox.Show("Estudiante no encontrado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar el estudiante: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            TxtIdEstudiante.Text = "";
            TxtNombre.Text = "";
            TxtGrado.Text = "";
            TxtAsingnarMateria.Text = "";
        }

        private void FrmEstudiante_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        

    }
}
