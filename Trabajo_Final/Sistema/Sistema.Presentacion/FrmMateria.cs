﻿using System;
using System.Windows.Forms;
using Sistema.Entidades;
using Sistema.Negocio;

namespace Sistema.Presentacion
{
    public partial class FrmMateria : Form
    {
        public FrmMateria()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                EMateria materia = new EMateria
                {
                    NombreMateria = TxtMateria.Text
                };

                NMateria.Guardar(0, "Nombre de la Materia");
                MessageBox.Show("Materia guardada exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar la materia: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                EMateria materia = new EMateria
                {
                    IdMateria = Convert.ToInt32(TxtIdMateria.Text),
                    NombreMateria = TxtMateria.Text
                };

                NMateria.Modificar(materia.IdMateria, materia.NombreMateria); 
                MessageBox.Show("Materia modificada exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al modificar la materia: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                int idMateria = Convert.ToInt32(TxtIdMateria.Text);
                NMateria.Eliminar(idMateria);
                MessageBox.Show("Materia eliminada exitosamente", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al eliminar la materia: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                int idMateria = Convert.ToInt32(TxtIdMateria.Text);
                EMateria materia = NMateria.Buscar(idMateria);

                if (materia != null)
                {
                    TxtMateria.Text = materia.NombreMateria;
                }
                else
                {
                    MessageBox.Show("Materia no encontrada", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar la materia: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            TxtIdMateria.Text = "";
            TxtMateria.Text = "";
        }

        private void FrmMateria_Load(object sender, EventArgs e)
        {

        }

        
    }
}
