﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.Presentacion
{
    public partial class FrmCrud : Form
    {
        public FrmCrud()
        {
            InitializeComponent();
        }

        private void BtnEstudiante_Click(object sender, EventArgs e)
        {
            FrmEstudiante frm = new FrmEstudiante();
            frm.Show();
        }

        private void BtnMateria_Click(object sender, EventArgs e)
        {
            FrmMateria frm = new FrmMateria();
            frm.Show();
        }

        private void BtnNota_Click(object sender, EventArgs e)
        {
            FrmNotas frm = new FrmNotas();
            frm.Show();
        }

        private void FrmCrud_Load(object sender, EventArgs e)
        {

        }
    }
}
